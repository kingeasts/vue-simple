const path = require('path')
const webpack = require('webpack')
const ROOT = path.resolve(__dirname, '..')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const glob = require('glob')

const files = glob.sync(path.resolve(__dirname, '..', 'public', '*.html'))

let plugins = [
  new CleanWebpackPlugin(['dist'], {
    root: ROOT,
    verbose: true
  }),
  new webpack.optimize.UglifyJsPlugin({
    compress: true
  }),
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor'
  }),
  new ExtractTextPlugin({
    filename: 'css/[name].css'
  })
]
for (let i = 0; i < files.length; i++) {
  const mat = files[i].match(/\w+\.html$/)

  let plug =  new HTMLWebpackPlugin({
    filename: mat[0],
    chunks: ['vendor', mat[0].replace('.html', '')],
    template: files[i],
    inject: true,
    hash: true
  })
  plugins.push(plug)
}

const config = {
  plugins: plugins
}

module.exports = config
