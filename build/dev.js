const path = require('path')
const webpack = require('webpack')
const ROOT = path.resolve(__dirname, '..')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const glob = require('glob')

const files = glob.sync(path.resolve(__dirname, '..', 'public', '*.html'))

let plugins = [
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin(),
  new CopyWebpackPlugin([
    {
      from: path.resolve(__dirname, '..', 'public'),
      to: path.resolve(ROOT, 'dist'),
      ignore: ['.*']
    }
  ])
]
for (let i = 0; i < files.length; i++) {
  const mat = files[i].match(/\w+\.html$/)

  let plug =  new HTMLWebpackPlugin({
    filename: mat[0],
    chunks: ['vendor', mat[0].replace('.html', '')],
    template: files[i],
    inject: true,
    hash: true
  })
  plugins.push(plug)
}

const config = {
  devtool: 'inline-source-map',
  devServer: {
    // historyApiFallback: true,
    historyApiFallback: {
      rewrites: [
        { from: /.*/, to: path.posix.join(ROOT, 'public', 'index.html') },
      ],
    },
    contentBase: false, // since we use CopyWebpackPlugin.
    // contentBase: path.posix.join(__dirname, '..', 'public'),
    compress: true,
    port: 8080,
    hot: true,
    inline: true,
    noInfo: false,
    clientLogLevel: 'warning'
  },
  output: {
    publicPath: '/'
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': path.join(__dirname, '..', 'src')
    }
  },
  plugins: plugins
}

module.exports = config
