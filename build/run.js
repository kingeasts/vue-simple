const merge = require('webpack-merge')
const commonConfig = require('./common.js')
const runtime = require(`./${process.env.NODE_ENV}.js`)

module.exports = merge(commonConfig, runtime)
