import Vue from 'vue'
import Router from 'vue-router'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'

Vue.use(Router)

const routes = new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      meta: {
        title: '登陆'
      },
      component: (resolve) => require(['./views/index.vue'], resolve)
    }
  ]
})

routes.beforeEach((to, from, next) => {
  nprogress.start()
  next()
})

routes.afterEach(() => {
  nprogress.done()
})

export default routes
