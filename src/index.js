import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import './assets/css/main.scss'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})